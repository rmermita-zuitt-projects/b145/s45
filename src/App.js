import { Container } from 'react-bootstrap'
import AppNavbar from './components/AppNavbar'
import {BrowserRouter as Router} from 'react-router-dom'
import {Routes, Route} from 'react-router-dom'
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import Home from './pages/Home'
import Courses from './pages/Courses'
import './App.css';
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import { UserProvider } from './UserContext'
import {useState, useEffect} from 'react'
import CourseView from './pages/CourseView'

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    // console.log(user)
    // console.log(localStorage)
    fetch ('https://pacific-beach-70439.herokuapp.com/users/details', 
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`
        }
      })
      .then(res => res.json())
      .then(data => {
        console.log(data)

        if (typeof data._id !== "undefined") {

         setUser({
          id:data._id,
          isAdmin:data.isAdmin
         })   
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
      })

  }, [])

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
      <Container>
        <Routes>
          <Route exact path= "/" element={<Home/>}/>
          <Route exact path= "/courses" element={<Courses/>}/>
          <Route exact path="/courses/:courseId" element={<CourseView/>}/>
          <Route exact path= "/register" element={<Register/>}/>
          <Route exact path= "/login" element={<Login/>}/>
          <Route exact path= "/logout" element={<Logout/>}/>
          <Route exact path="*" element={<Error/>}/>
        </Routes>
      {/*  <Banner/>
        <Highlights/>*/}
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;
