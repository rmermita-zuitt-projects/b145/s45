import { Row, Col, Card } from 'react-bootstrap'
export default function Highlights() {
	return (
		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn from home</h2>
						</Card.Title>
						<Card.Text>
							Lorem, ipsum, dolor sit amet consectetur adipisicing elit. Voluptatum, molestias? Debitis praesentium, fugit sed eaque obcaecati vel aliquid impedit ullam natus voluptates molestiae soluta saepe est eos, iusto, maiores et.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Lorem, ipsum, dolor sit amet consectetur adipisicing elit. Voluptatum, molestias? Debitis praesentium, fugit sed eaque obcaecati vel aliquid impedit ullam natus voluptates molestiae soluta saepe est eos, iusto, maiores et.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Be part of our community!</h2>
						</Card.Title>
						<Card.Text>
							Lorem, ipsum, dolor sit amet consectetur adipisicing elit. Voluptatum, molestias? Debitis praesentium, fugit sed eaque obcaecati vel aliquid impedit ullam natus voluptates molestiae soluta saepe est eos, iusto, maiores et.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}