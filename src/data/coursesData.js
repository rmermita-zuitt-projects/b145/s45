const coursesData = [
	{
		id : "wdc001",
		name : "PHP- Laravel",
		description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ad dolores, doloribus numquam nobis omnis quibusdam deserunt obcaecati fugit vitae error iure id eaque harum sed possimus, eligendi qui voluptates repellat!",
		price : 45000,
		onOffer : true
	},
	{
		id : "wdc002",
		name : "Python- Django",
		description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ad dolores, doloribus numquam nobis omnis quibusdam deserunt obcaecati fugit vitae error iure id eaque harum sed possimus, eligendi qui voluptates repellat!",
		price : 50000,
		onOffer : true
	},
	{
		id : "wdc003",
		name : "Java- Springboot",
		description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ad dolores, doloribus numquam nobis omnis quibusdam deserunt obcaecati fugit vitae error iure id eaque harum sed possimus, eligendi qui voluptates repellat!",
		price : 55000,
		onOffer : true
	},


]

export default coursesData;
